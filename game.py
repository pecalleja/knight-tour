from collections import defaultdict
from functools import cmp_to_key

MOVE_OFFSETS = (
              (-1, -2), (1, -2),
    (-2, -1),                     (2, -1),
    (-2,  1),                     (2,  1),
              (-1,  2), (1,  2),
)


def warnsdorffs_heuristic(graph):
    def comparator(a, b):
        return len(graph[a]) - len(graph[b])
    return comparator


class KnightBoard:
    def __init__(self, x, y):
        self.length_x, self.length_y = x, y
        self.total_steps = self.length_y * self.length_x
        self.graph = defaultdict(set)
        self.build_graph()
        self.board = [["_"] * self.length_x for _ in range(self.length_y)]
        self.heuristic = lambda graph: cmp_to_key(warnsdorffs_heuristic(graph))

    def legal_moves_from(self, row, col):
        for row_offset, col_offset in MOVE_OFFSETS:
            move_row, move_col = row + row_offset, col + col_offset
            if 0 <= move_row < self.length_x and 0 <= move_col < self.length_y:
                yield move_row, move_col

    def remove_point(self, point):
        self.graph.pop(point, None)
        empty_keys = []
        for key, value in self.graph.items():
            if point in value:
                value.remove(point)
            if not value:
                empty_keys.append(key)
        for empty in empty_keys:
            self.graph.pop(empty, None)

    def get_available_moves(self, row, col):
        legal_moves = self.graph.get((row, col))
        available_moves = defaultdict(int)
        if legal_moves:
            for x, y in legal_moves:
                if self.board[x][y] not in ("*", "X"):
                    available_moves[(x, y)] = len(self.graph.get((x, y))) - 1
        return available_moves

    def add_edge(self, vertex_a, vertex_b):
        self.graph[vertex_a].add(vertex_b)
        self.graph[vertex_b].add(vertex_a)

    def build_graph(self):
        for row in range(self.length_x):
            for col in range(self.length_y):
                for to_row, to_col in self.legal_moves_from(row, col):
                    self.add_edge((col, row), (to_col, to_row))
        return self.graph

    def check_solution(self, starting_point):
        return self.traverse([], starting_point)

    def traverse(self, path, current_vertex, ):
        def first_true(sequence):
            for item in sequence:
                if item:
                    return item
            return []

        if len(path) + 1 == self.total_steps:
            return path + [current_vertex]

        yet_to_visit = self.graph[current_vertex] - set(path)
        if not yet_to_visit:
            return []

        next_vertices = sorted(yet_to_visit, key=self.heuristic(self.graph))
        return first_true(self.traverse(path + [current_vertex], vertex) for vertex in next_vertices)

    def _place_holder(self):
        return len(str(self.total_steps))

    def _border_length(self, placeholder):
        return self.length_x * (placeholder + 1) + 3

    def print_board(self):
        placeholder = self._place_holder()
        border_length = self._border_length(placeholder)

        border = "-"*border_length
        print(f" {border}")
        for i, all_row in enumerate(self.board):
            row_number = str(self.length_y - i).rjust(placeholder)
            row = f"{row_number}|"
            for j, element in enumerate(all_row):
                row += " " + element.rjust(placeholder, "_" if element == "_" else " ")
            row += " |"
            print(row)
        print(f" {border}")
        columns = "    "
        for element in range(self.length_x):
            number = str(element+1).rjust(placeholder)
            columns += number + " "
        print(columns)


class GamePuzzle:
    board: KnightBoard
    start_point: tuple

    @staticmethod
    def valid_input_values(values) -> tuple:
        if len(values) != 2:
            raise ValueError
        x = int(values[0])
        y = int(values[1])
        if not (x or y):
            raise ValueError
        return x, y

    def _amount_squares(self):
        amount = 0
        for row in self.board.board:
            for cell in row:
                if cell in ("*", "X"):
                    amount += 1
        return amount

    def start(self, solution=None):
        self.board = KnightBoard(*self.get_board_dimensions())
        self.start_point = self.get_starting_point()
        if solution is None:
            while True:
                manual = input("Do you want to try the puzzle?")
                if manual in ("y", "n"):
                    break
                else:
                    print("Invalid input!")
            solution = self.board.check_solution(self.start_point)
            if solution and len(solution) > 1:
                if manual == "y":
                    self.game_loop()
                else:
                    self.show_solution(solution)
            else:
                print("No solution exists!")
        else:
            self.show_solution(solution)

    def valid_next_move(self, previous_point):
        x, y = False, False
        print("Enter your next move:")
        while not all([x, y]):
            result = input().split()
            try:
                x, y = self.valid_input_values(result)
                if not (1 <= x <= self.board.length_x and 1 <= y <= self.board.length_y) or not (x or y):
                    raise ValueError
                next_move_point = self.board.length_y - y, x - 1
                legal_moves = self.board.graph.get(previous_point)
                if not legal_moves or next_move_point not in legal_moves:
                    raise ValueError
                next_move_value = self.board.board[next_move_point[0]][next_move_point[1]]
                if next_move_value in ["*", "X"]:
                    raise ValueError
                return next_move_point
            except ValueError:
                print("Invalid move!", end="")
                x, y = False, False
                continue

    def set_possible_moves(self, moves):
        for move, value in moves.items():
            self.board.board[move[0]][move[1]] = str(value)

    def clear_possible_moves(self, moves):
        for move, value in moves.items():
            if self.board.board[move[0]][move[1]] not in ("*", "X"):
                self.board.board[move[0]][move[1]] = "_"

    def game_loop(self):
        self.board.board[self.start_point[0]][self.start_point[1]] = "X"
        available_moves = self.board.get_available_moves(*self.start_point)
        self.set_possible_moves(available_moves)
        self.board.print_board()
        self.clear_possible_moves(available_moves)
        previous_point = self.start_point
        while True:
            next_move = self.valid_next_move(previous_point)
            self.board.board[next_move[0]][next_move[1]] = "X"
            self.board.board[previous_point[0]][previous_point[1]] = "*"
            self.board.remove_point(previous_point)
            available_moves = self.board.get_available_moves(*next_move)
            self.set_possible_moves(available_moves)
            previous_point = next_move
            self.board.print_board()
            if not available_moves:
                amount_squares = self._amount_squares()
                if amount_squares == self.board.total_steps:
                    print("What a great tour! Congratulations!")
                else:
                    print("No more possible moves!")
                    print(f"Your knight visited {amount_squares} squares!")
                break
            self.clear_possible_moves(available_moves)

    def show_solution(self, solution):
        print("Here's the solution!")
        for i, value in enumerate(solution):
            self.board.board[value[1]][value[0]] = str(i+1)
        self.board.print_board()

    def get_starting_point(self):
        x, y = False, False
        while not all([x, y]):
            result = input("Enter the knight's starting position:").split()
            try:
                x, y = self.valid_input_values(result)
                if not (1 <= x <= self.board.length_x and 1 <= y <= self.board.length_y) or not (x or y):
                    raise ValueError
                return self.board.length_y - y, x - 1
            except ValueError:
                print("Invalid position!")
                continue

    def get_board_dimensions(self):
        x, y = False, False
        while not all([x, y]):
            result = input("Enter your board dimensions:").split()
            try:
                x, y = self.valid_input_values(result)
                if not x or not y:
                    raise ValueError
                return x, y
            except ValueError:
                print("Invalid dimensions!")
                continue


GamePuzzle().start()
